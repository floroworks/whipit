package com.dibidi2x.games.whipit;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.TimeUtils;

public class WhipIt extends ApplicationAdapter implements InputProcessor{
    SpriteBatch batch;

    Texture whip_on;
    Texture whip_off;
    Texture wapak;

    Sprite whip_sprite;
    Sprite wapak_sprite;

    BitmapFont font;
    GlyphLayout font_glyph;

    static final String WHIP_OFF_MESSAGE = "PRESS AND HOLD WHIP";
    static final String WHIP_ON_MESSAGE = "NOW WHIP IT!";
    String message;

    static final float WHIP_THRESHOLD = 3f;
    static final long WHIP_DURATION = 2000; // 2 seconds
    static final long WHIP_INTERVAL = 850; // 0.85 second
    long lastWhipped;

    boolean enabled;

    float dev_width;
    float dev_height;

    OrthographicCamera cam;

    Sound whip_sound;

    @Override
    public void create () {

        // Initialize textures
        whip_off = new Texture(Gdx.files.internal("images/whip_off.png"));
        whip_on  = new Texture(Gdx.files.internal("images/whip_on.png"));
        wapak    = new Texture(Gdx.files.internal("images/wapak.png"));

        // Set texture filters to remove the jaggies
        whip_off.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        whip_on.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        wapak.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        // Initialize sprites
        whip_sprite = new Sprite(whip_off); // use whip_off as default image of sprite
        wapak_sprite = new Sprite(wapak);

        // Get the device width and height to be used in camera
        dev_width = Gdx.graphics.getWidth();
        dev_height = Gdx.graphics.getHeight();

        // Create a camera using full width and height of device
        cam = new OrthographicCamera(dev_width, dev_height);
        // set camera position on center of device
        cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0);

        // Set sizes and positions of sprites relative to the device dimensions
        // whip width is 90% of device width
        // whip height is 80% of device height
        whip_sprite.setSize(dev_width - (dev_width * 0.1f), dev_height - (dev_height * 0.2f));
        // position whip sprite center bottom of screen
        whip_sprite.setPosition(dev_width / 2 - whip_sprite.getWidth() / 2, 0);
        // wapak width is 90% of device width
        // wapak height is 30% of device height
        wapak_sprite.setSize(dev_width - (dev_width * 0.1f), dev_height - (dev_height * 0.7f));
        // position wapak sprite center horizontal/vertical
        wapak_sprite.setPosition(dev_width / 2 - wapak_sprite.getWidth() / 2,
                dev_height / 2 - (wapak_sprite.getHeight() / 2));

        // Generate font
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                Gdx.files.internal("fonts/LuckiestGuy.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter param = new
                FreeTypeFontGenerator.FreeTypeFontParameter();
        // font size is 45 pt of the device width relative to 480 pt of screen width
        param.size = (int) (dev_width * 45 / 480);
        // filter for scaling font
        param.minFilter = Texture.TextureFilter.Linear;
        param.magFilter = Texture.TextureFilter.MipMapLinearNearest;
        param.color = Color.BLACK;
        font = generator.generateFont(param);
        generator.dispose();

        font_glyph = new GlyphLayout();

        // default message is whip off
        message = WHIP_OFF_MESSAGE;
        // set last whipped in the past so initial state will not be "whipped"
        lastWhipped = TimeUtils.millis() - WHIP_DURATION * 2;

        batch = new SpriteBatch();

        // Register this class as listener of inputs
        Gdx.input.setInputProcessor(this);

        whip_sound = Gdx.audio.newSound(Gdx.files.internal("audio/bullwhip.wav"));

        // enabled default to false
        enabled = false;
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
        whip_off.dispose();
        whip_on.dispose();
        wapak.dispose();
        whip_sound.dispose();
    }

    @Override
    public void render () {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // update camera
        cam.update();
        // set Sprite Batch to use the camera as projection matrix
        batch.setProjectionMatrix(cam.combined);

        // Set the glyph's font and message
        font_glyph.setText(font, message);

        // Detect shake if enabled
        if (enabled && shaked()) {
            // Check if just shaked, if not proceed, else skip
            if (TimeUtils.timeSinceMillis(lastWhipped) > WHIP_INTERVAL) {
                // whipped! take note of current time
                lastWhipped = TimeUtils.millis();
                //whipIt(WHIP_INTERVAL);
                whipIt();
            }
        }

        batch.begin();
        // Check first if recently shaked, then draw appropriate sprites
        if (TimeUtils.timeSinceMillis(lastWhipped) < WHIP_DURATION) {
            // Gray color - #434343 in hex
            Gdx.gl.glClearColor(43 / 255f, 43 / 255f, 43 / 255f, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            // draw wapak sprite
            wapak_sprite.draw(batch);
        } else {
            // draw the message on top of screen, horizontally centered, vertically centered on top 10%
            font.draw(batch, font_glyph, cam.viewportWidth / 2 - font_glyph.width / 2,
                    cam.viewportHeight - (cam.viewportHeight * 0.1f));
            // draw the whip sprite, no need to position already done in create
            whip_sprite.draw(batch);
        }
        batch.end();
    }

    private boolean shaked() {

        // Get the acceleration values on all axes (divide 9.8m/s^2 to remove earth's gravity)
        float accelX = Gdx.input.getAccelerometerX() / 9.8f;
        float accelY = Gdx.input.getAccelerometerY() / 9.8f;
        float accelZ = Gdx.input.getAccelerometerZ() / 9.8f;

        // Compute for g-force
        // square root of sum of squares on all axes
        float gForce = (float) Math.sqrt((accelX * accelX) +
                (accelY * accelY) +
                (accelZ * accelZ));

        // If calculated g-force is greater than threshold, shaked is true
        return (gForce > WHIP_THRESHOLD);
    }

    private void whipIt() {
        whip_sound.play(1f);
        // Vibrate pattern - 200ms on, 100ms off, 500ms on, 100ms off, 800ms on
        Gdx.input.vibrate(new long[] {0, 200, 100, 500, 100, 800}, -1);
    }


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // Reset the font glyph, we'll use another message
        font_glyph.reset();
        // Change the message to be displayed
        message = WHIP_ON_MESSAGE;

        // Change sprite texture of whip_sprite
        // Set to ON
        whip_sprite.setTexture(whip_on);

        // enable whip
        enabled = true;

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // Reset the font glyph, we'll use another message
        font_glyph.reset();
        // Change the message to be displayed
        message = WHIP_OFF_MESSAGE;

        // Change sprite texture of whip_sprite
        // Set to OFF
        whip_sprite.setTexture(whip_off);

        // disable whip
        enabled = false;

        return true;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}